#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controllers;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! the client locale is "+ locale.toString());
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String guess(WebRequest webRequest, RedirectAttributes redirectAttributes, @RequestParam int value) {
		Integer myNumber = (Integer) webRequest.getAttribute(CommonInterceptor.ATTR_MY_NUMBER, WebRequest.SCOPE_SESSION);
		if (value == myNumber) {
			fillMessage(redirectAttributes, "You got it! Reload to try again.");
			webRequest.removeAttribute(CommonInterceptor.ATTR_MY_NUMBER, WebRequest.SCOPE_SESSION);
			redirectAttributes.addFlashAttribute("success", Boolean.TRUE);
		}
		else if (value > myNumber) {
			fillMessage(redirectAttributes, "It's lower than "+value);
		}
		else {
			fillMessage(redirectAttributes, "It's higher than "+value);
		}
		return "redirect:/";
	}

	private void fillMessage(RedirectAttributes requestAttributes, String msg) {
		requestAttributes.addFlashAttribute("message", msg);
	}
	
}
