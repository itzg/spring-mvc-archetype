#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controllers;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class CommonInterceptor implements HandlerInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(CommonInterceptor.class);
	
	public static final String ATTR_MY_NUMBER = "myNumber";
	public static final String ATTR_GUESS_MAX = "guessMax";
	
	private Random rand = new Random();

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.info("Intercepting", handler);
		
		int guessMax = Integer.parseInt(request.getSession().getServletContext().getInitParameter("guessMax"));
		request.setAttribute(ATTR_GUESS_MAX, guessMax);
		if (request.getSession().getAttribute(ATTR_MY_NUMBER) == null) {
			request.getSession().setAttribute(ATTR_MY_NUMBER, rand.nextInt(guessMax)+1);
		}
//		request.setAttribute(ATTR_MY_NUMBER, 678);
		return true;
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
