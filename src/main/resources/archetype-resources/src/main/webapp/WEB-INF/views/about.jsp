#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<html>
<head>
	<title>About</title>
</head>
<body>
<h1>
	About your application  
</h1>
<p>You got here without a controller.</p>
<p>...but I still picked the number <c:out value="${symbol_dollar}{myNumber}"></c:out></p>
</body>
</html>
