#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Hello world!</h1>

	<p>The time on the server is ${symbol_dollar}{serverTime}.</p>

	<p>
	<div>${symbol_dollar}{message}</div>

	<c:if test="${symbol_dollar}{!success}">
		<form method="post">

			<div>
				<label for="value">Guess my number. It's between 1 and
					${symbol_dollar}{guessMax}:</label> <input name="value" id="value" type="number"></input>
			</div>
			<button type="submit">Guess</button>
		</form>
	</c:if>

	</p>

</body>
</html>
